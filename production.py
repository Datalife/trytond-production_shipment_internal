# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields, ModelView
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Bool, Eval, Not, Id
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateTransition, StateView, Button, StateAction

__all__ = ['Configuration', 'Production', 'ProductionShipment', 'ProductionShipmentStart',
           'ProductionShipmentData', 'ProductionShipmentConfirm']

__metaclass__ = PoolMeta


class Configuration:
    __name__ = 'production.configuration'

    distribute_shipment = fields.Boolean('Distribute shipment along locations',
                                         help='Determines if internal shipments must be distribute along locations '
                                              'due to storage space availability.')


class Production:
    __name__ = 'production'

    shipments_internal = fields.One2Many('stock.shipment.internal', 'production', 'Shipments')

    @classmethod
    def __setup__(cls):
        super(Production, cls).__setup__()
        cls._error_messages.update(
            {'wrong_production_state': 'Production "%(production)s" must be in done state.',
             'shipment_unfinished': 'There are already internal shipments not finished for Production "%s".'})
        cls._buttons.update({
            'shipment_wizard': {'invisible': Eval('state') != 'done',
                                'readonly': ~Eval('groups', []).contains(Id('stock', 'group_stock'))}
        })

    @classmethod
    @ModelView.button_action('production_shipment_internal.wizard_shipment')
    def shipment_wizard(cls, productions):
        pass

    @classmethod
    def ship_try(cls, productions):
        pool = Pool()
        Shipment = pool.get('stock.shipment.internal')
        for production in productions:
            if production.state != 'done':
                cls.raise_user_error('wrong_production_state', {'production': production.rec_name})
            shipments = Shipment.search([('production', '=', production.id),
                                         ('state', 'not in', ['assigned', 'waiting', 'done'])])
            if shipments and len(shipments) > 0:
                cls.raise_user_error('shipment_unfinished', (production.rec_name, ))


#TODO: chequear que no hay albaranes internos posteriores a planned_date indicada por el usuario
class ProductionShipment(Wizard):
    """Ship Production"""
    __name__ = 'production.shipment'

    start = StateTransition()
    date = StateView('production.shipment.start', 'production_shipment_internal.shipment_start_view_form',
                     [Button('Cancel', 'end', 'tryton-cancel'),
                      Button('Next', 'location', 'tryton-go-next', default=True)])
    location = StateView('production.shipment.data', 'production_shipment_internal.shipment_data_view_form',
                         [Button('Cancel', 'end', 'tryton-cancel'),
                          Button('Next', 'confirm', 'tryton-go-next', default=True)])
    confirm = StateView('production.shipment.confirm', 'production_shipment_internal.shipment_confirm_view_form',
                        [Button('Cancel', 'end', 'tryton-cancel'),
                         Button('Ok', 'do', 'tryton-ok', default=True)])
    do = StateTransition()
    open_ = StateAction('stock_shipment_internal_grouping.act_shipment_internal_group_form')

    def _get_model(self):
        pool = Pool()
        _Production = pool.get('production')
        return _Production(Transaction().context['active_id'])

    def _get_configuration(self):
        pool = Pool()
        Configuration = pool.get('production.configuration')
        return Configuration(1)

    def _must_distribute(self):
        return getattr(self._get_configuration(), 'distribute_shipment', False)

    def transition_start(self):
        Production.ship_try([self._get_model()])
        return 'date'

    def default_date(self, fields):
        production = self._get_model()
        return {'planned_date': production.effective_date}

    def default_location(self, fields):
        defaults = {'distribute': self._must_distribute()}
        production = self._get_model()

        if not production:
            return defaults
        defaults['stock'] = production.get_stock_by_location(stock_date_end=self.date.planned_date)
        return defaults

    def default_confirm(self, fields):
        pool = Pool()
        Confirm = pool.get('production.shipment.confirm')

        new_moves = self.explode_moves()
        result = []
        for m in new_moves:
            result.append(Confirm.explode_move_values(m))
        return {'moves': result}

    def explode_moves(self):
        pool = Pool()
        Confirm = pool.get('production.shipment.confirm')
        Move = pool.get('stock.move')

        moves = []
        for m in self.location.stock:
            moves.append(Confirm.explode_move(m,
                                              self.date.planned_date,
                                              self.location.to_location))
        # create new movements
        if self.location.distribute:
            return Move.distribute(moves, self.location.sequence_order)
        return moves

    def transition_do(self):
        pool = Pool()
        Group = pool.get('stock.shipment.internal.group')

        production = self._get_model()

        moves = self.confirm.moves

        shipment_groups = {}

        # groups movements with from-to location criteria to create n-shipments
        for m in moves:
            shipment_groups.setdefault((m.from_location.id, m.to_location.id), [])
            shipment_groups[(m.from_location.id, m.to_location.id)].append(m)

        shipments = []
        for key, values in shipment_groups.iteritems():
            changes = [('create', [self.confirm.explode_move_values(v) for v in values])]
            shipment = {'planned_date': self.date.planned_date,
                        'effective_date': self.date.planned_date,
                        'state': 'draft',
                        'production': production.id,
                        'from_location': key[0],
                        'to_location': key[1],
                        'moves': changes}
            shipments.append(shipment)

        Group.create([{'reference': production.code,
                      'company': production.company.id if production.company else None,
                      'shipments': [['create', shipments]]}])
        return 'open_'

    def do_open_(self, action):
        pool = Pool()
        Group = pool.get('stock.shipment.internal.group')

        production = self._get_model()
        group, = Group.search([('production', '=', production.id)],
                              order=[('id', 'DESC')], limit=1)

        action['name'] += " - " + group.rec_name
        action['views'].reverse()
        return action, {'res_id': [group.id]}

    def transition_open_(self):
        return 'end'


class ProductionShipmentStart(ModelView):
    """Shipment data"""
    __name__ = 'production.shipment.start'

    planned_date = fields.Date('Planned Date', required=True)


class ProductionShipmentData(ModelView):
    """Shipment data"""
    __name__ = 'production.shipment.data'

    distribute = fields.Boolean('Distribute along locations',
                                help='Distributes products along locations considering available storage space.',
                                states={'readonly': Eval('distribute_readonly', False)},
                                depends=['distribute_readonly'])
    distribute_readonly = fields.Function(fields.Boolean('Distribute Readonly'),
                                          'get_distribute_readonly')
    to_location = fields.Many2One('stock.location', 'To Location',
                                  required=True,
                                  domain=[('type', 'in', ['storage', 'lost_found'])])
    sequence_order = fields.Selection([('ascendant', 'Ascendant'),
                                       ('descendant', 'Descendant')],
                                      'Location Sequence order',
                                      states={'invisible': Not(Bool(Eval('distribute')))},
                                      depends=['distribute'],
                                      required=True)
    stock = fields.One2Many('stock.lot.location-quantity', None, 'Stock',
                            readonly=True)

    @staticmethod
    def default_sequence_order():
        return 'ascendant'

    @classmethod
    def default_distribute(cls):
        Configuration = Pool().get('production.configuration')
        configuration = Configuration(1)
        return getattr(configuration, 'distribute_shipment', False)

    @staticmethod
    def default_distribute_readonly():
        return False

    def get_distribute_readonly(self, name):
        return False


class ProductionShipmentConfirm(ModelView):
    """Shipment Confirmation"""
    __name__ = 'production.shipment.confirm'

    moves = fields.One2Many('stock.move', None, 'Moves', readonly=True)

    @classmethod
    def movements(cls, planned_date, stock, to_location):
        changes = []
        for m in stock:
            new_move = cls.explode_move(m, planned_date, to_location)
            changes.append(cls.explode_move_values(new_move))
        return {'moves': changes}

    @classmethod
    def explode_move(cls, origin, planned_date, to_location):
        pool = Pool()
        Move = pool.get('stock.move')
        Product = pool.get('product.product')

        product = Product(origin.product.id)
        production = cls._get_model()
        move = Move(
            planned_date=planned_date,
            product=origin.product,
            uom=product.default_uom,
            quantity=origin.quantity,
            from_location=origin.location,
            to_location=to_location,
            lot=origin.lot,
            company=production.company,
            currency=production.company.currency if production.company else None,
            state='draft')
        return move

    @classmethod
    def explode_move_values(cls, move):
        pool = Pool()
        Move = pool.get('stock.move')
        values = {}
        for field_name, field in Move._fields.iteritems():
            try:
                value = getattr(move, field_name)
            except AttributeError:
                continue
            if value and field._type in ('many2one', 'one2one'):
                values[field_name] = value.id
            else:
                values[field_name] = value
        return values

    @classmethod
    def _get_model(cls):
        pool = Pool()
        Production = pool.get('production')
        return Production(Transaction().context['active_id'])

